package org.academiadecodigo.asynctomatics.line;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class WordReader implements Iterable<String> {

    private String file;
    private String[] arrResult;

    // constructor
    public WordReader(String file) throws IOException {
        this.file = file;
    }

    public String[] readFileByWord(String file) throws IOException {

        FileReader fileReader = new FileReader(file);
        BufferedReader bReader = new BufferedReader(fileReader);

        String line = "";
        String result = "";

        line = bReader.readLine();
//        result += line;

        bReader.close();

        arrResult = line.split(" ");
        for (String r: arrResult) {
            System.out.println(r);
        }

        return arrResult;
    }

    @Override
    public Iterator<String> iterator() {

        return new Iterator<String>() {

            int currentWord = 0;

            @Override
            public boolean hasNext() {
                return currentWord < arrResult.length;
            }

            @Override
            public String next() {
                if (!hasNext()) {
                    throw new NoSuchElementException();
                }
                // print the first word
                String var = arrResult[currentWord];
                currentWord++;
                return var;
            }
        };
    }
}
