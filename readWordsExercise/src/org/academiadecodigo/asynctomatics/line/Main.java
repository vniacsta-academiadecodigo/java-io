package org.academiadecodigo.asynctomatics.line;

import org.academiadecodigo.asynctomatics.word.WordReader;

import java.io.IOException;
import java.util.Iterator;

public class Main {

    public static void main(String[] args) throws IOException {

        String file = "example.txt";

        try {
            org.academiadecodigo.asynctomatics.word.WordReader wordReader = new WordReader(file);

            wordReader.readFileByWord(file);

//            for (int i = 0; i < arrWords.length; i++) {
//                System.out.println(arrWords[i]);
//            }
//
//            Iterator<String> iterator = wordReader.iterator();
//
//            while (iterator.hasNext()) {
//                System.out.println(iterator.next());
//            }

        } catch (IOException ex) {
            System.out.println("Can not read file: " + file);
        }
    }
}
