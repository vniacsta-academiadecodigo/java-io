package org.academiadecodigo.asynctomatics.word;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class WordReader implements Iterable<String> {

    private String file;
    private String[] arrResult;

    // constructor
    public WordReader(String file) throws IOException {
        this.file = file;
    }

    public String[] readFileByWord(String file) throws IOException {

        FileReader fileReader = new FileReader(file);
        BufferedReader bReader = new BufferedReader(fileReader);

        String line = "";
        String result = "";

        while ((line = bReader.readLine()) != null) {
            result += line + '\n';
        }

        bReader.close();

        arrResult = result.split(" ");

        return arrResult;
    }

    @Override
    public Iterator<String> iterator() {

        return new Iterator<String>() {

            int currentWord = 0;

            @Override
            public boolean hasNext() {
                return currentWord < arrResult.length;
            }

            @Override
            public String next() {
                if (!hasNext()) {
                    throw new NoSuchElementException();
                }
                // to print the first word
                String word = arrResult[currentWord];
                currentWord++;
                return word;
            }
        };
    }
}
