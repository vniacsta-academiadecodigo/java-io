package org.academiadecodigo.asynctomatics;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {

        String fileName = "example.txt";
        try {
            FileInputStream input = new FileInputStream(fileName);
            FileOutputStream output = new FileOutputStream("example2.txt");

            byte[] buffer = new byte[1024];

            int lengthRead;

            while ((lengthRead = input.read(buffer)) != -1) {
                output.write(buffer, 0, lengthRead);
                output.flush();
                System.out.println(lengthRead);
            }

            input.close();
            output.close();

        } catch (IOException ex) {
            System.out.println("Error reading file " + fileName);
        }
    }
}
